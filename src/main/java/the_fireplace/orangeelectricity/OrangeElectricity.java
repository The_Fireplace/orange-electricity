package the_fireplace.orangeelectricity;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * @author The_Fireplace
 */
@Mod(modid=OrangeElectricity.MODID, name=OrangeElectricity.MODNAME)
public class OrangeElectricity {
	public static final String MODID = "orangeelectricity";
	public static final String MODNAME = "Orange Electricity";
	public static String VERSION;

	@SuppressWarnings("unused")
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event){

	}
}
